import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ListItem from '../components/ListItem';

const styles = theme => ({
    root: {
        flexGrow: 1,
        'overflow': 'hidden',
        'padding': '50px'
    },
    heading: {
        'text-align': 'center'
    }
});

class ContentLayout extends React.Component {
    render() {
        const {classes, moviesList, error} = this.props;
        return (
            <div className={classes.root}>
                {error.Error ? (
                    <Grid
                        className={classes.heading}
                        container
                        spacing={24}
                    >
                        <Grid item xs={12}>
                            <h1 className="error-message">{error.Error}</h1>
                        </Grid>
                    </Grid>
                ) : null}
                <Grid container spacing={24}>
                    {moviesList.map(item => (
                        <Grid key={item.imdbID}
                              item
                              lg={3}
                              md={4}
                              sm={6}
                              xs={12}
                              className={classes.items}>
                            <ListItem data={item}/>
                        </Grid>
                    ))}
                </Grid>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    moviesList: state.moviesList.list,
    error: state.moviesList.error
});

ContentLayout.propTypes = {
    classes: PropTypes.object.isRequired,
    moviesList: PropTypes.array.isRequired,
    error: PropTypes.object.isRequired,
};

const Container = withStyles(styles)(ContentLayout);

export default connect(
    mapStateToProps
)(Container)
