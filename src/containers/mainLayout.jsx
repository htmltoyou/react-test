import React, { Component } from 'react';

import Header from '../components/Header';
import Content from './contentlayout';
import ModalDelete from '../components/modals/delete';
import AddEdit from '../components/modals/addEdit';

export default class App extends Component {
    render() {
        return (
            <div>
                <Header />
                <Content />
                <ModalDelete />
                <AddEdit />
            </div>
        );
    }
}
