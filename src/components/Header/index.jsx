import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { searchData, openAddEditModal } from '../../actions';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';

const styles = theme => ({
    root: {
        width: '100%',
        'background-color': '#7744a2'
    },
    grow: {
        flexGrow: 1,
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit * 3,
            width: 'auto',
        },
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
        padding: '5px 10px'
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class Header extends React.Component {
    state = {
        title: '',
        year: ''
    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value.trim(),
        });
    };

    handleSearch = event => {
        event.preventDefault();
        this.props.searchData(this.state);
    };

    handleAdd = event => {
        event.preventDefault();
        this.props.openAddEditModal({});
    };

    render() {
        const { classes} = this.props;
        const { title, year } = this.state;
        return (
            <div className={classes.root}>
                <AppBar position="static" className={classes.root}>
                    <Toolbar>
                        <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                            Herolo
                        </Typography>
                        <div className={classes.search + ' row'}>
                            <InputBase
                                value={title}
                                onChange={this.handleChange('title')}
                                placeholder="Title"
                                classes={{
                                    root: classes.inputRoot
                                }}
                            />
                        </div>
                        <div className={classes.search  + ' row'}>
                            <InputBase
                                value={year}
                                onChange={this.handleChange('year')}
                                placeholder="Year"
                                classes={{
                                    root: classes.inputRoot
                                }}
                            />
                        </div>
                        <div className={classes.grow} />
                        <div>
                            <Button
                                onClick={this.handleSearch}
                                variant="contained"
                                color="secondary"
                                className={classes.button}>
                                <SearchIcon className={classes.leftIcon} />
                                Search
                            </Button>
                            <Button
                                onClick={this.handleAdd}
                                variant="contained"
                                color="secondary"
                                className={classes.button}>
                                <AddIcon className={classes.leftIcon} />
                                Add
                            </Button>
                        </div>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapDispatchToProps = dispatch => ({
    searchData: data => dispatch(searchData(data)),
    openAddEditModal: item => dispatch(openAddEditModal(item))
});

const Container = withStyles(styles)(Header);

export default connect(
    null,
    mapDispatchToProps
)(Container);
