import React from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import {closeDeleteModal, deleteItem} from "../../actions";


class ModalDelete extends React.Component {

    handleClose = () => {
        this.props.closeModal();
    };

    handleDelete = () => {
        this.props.deleteItem(this.props.id);
        this.props.closeModal();
    };

    render() {
        const {isOpen} = this.props;
        return (
            <div>
                <Dialog
                    open={isOpen}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Delete Item?"}</DialogTitle>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Close
                        </Button>
                        <Button onClick={this.handleDelete} color="primary" autoFocus>
                            Delete
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        id: state.modalDelete.id,
        isOpen: state.modalDelete.isOpen || false,
    }
}

const mapDispatchToProps = dispatch => ({
    closeModal: () => dispatch(closeDeleteModal()),
    deleteItem: id => dispatch(deleteItem(id))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ModalDelete);
