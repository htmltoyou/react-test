import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import {addItem, closeAddEditModal, updateItem} from "../../actions";
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        width: '100%',
    },
    dense: {
        marginTop: 19,
    },
    heading: {
        'text-align': 'center'
    },
    error: {
        'color': 'red'
    }
});

class ModalAddEdit extends React.Component {
    state = {
        itemToEdit: {}
    };

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState(nextProps);
    }

    handleClose = () => {
        this.props.closeModal();
    };

    handleSave = () => {
        const newTitle = JSON.stringify(this.state.itemToEdit.Title).replace(/[^a-zA-Z0-9\s]/g, '').toLowerCase().replace(/\b\w/g, l => l.toUpperCase());
        this.setState({
            itemToEdit: {
                ...this.state.itemToEdit,
                Title: newTitle
            }
        },
            () => {
                this.props.type ? this.props.updateItem(this.state.itemToEdit) : this.props.addItem(this.state.itemToEdit);
            }
        );
    };

    handleChange = name => event => {
        this.setState({
            itemToEdit: {
                ...this.state.itemToEdit,
                [name]: event.target.value
            }
        });
    };

    render() {
        const {classes, itemModal, type, error} = this.props;
        const {itemToEdit} = this.state;
        return (
            <div>
                <Dialog
                    open={itemModal}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle
                        className={classes.heading}
                        id="alert-dialog-title"
                    >{type ? 'Edit Item' : "Add new Item"}</DialogTitle>
                    <ValidatorForm
                        onSubmit={this.handleSave}
                    >
                        <DialogContent>
                            {error.name ? (
                                <p className={classes.error}>{error.name}</p>
                            ): null}
                            <TextValidator
                                label="Title"
                                className={classes.textField}
                                value={itemToEdit.Title}
                                validators={['required']}
                                errorMessages={['this field is required and can\'t be empty']}
                                onChange={this.handleChange('Title')}
                                margin="normal"
                                name="Title"
                            />
                            <TextValidator
                                label="Year"
                                type="number"
                                className={classes.textField}
                                value={itemToEdit.Year}
                                validators={['required', 'isNumber']}
                                errorMessages={['this field is required, only numbers accepted']}
                                onChange={this.handleChange('Year')}
                                margin="normal"
                                name="Year"
                            />
                            <TextValidator
                                label="Runtime"
                                className={classes.textField}
                                value={itemToEdit.Runtime}
                                onChange={this.handleChange('Runtime')}
                                margin="normal"
                                name="Runtime"
                            />
                            <TextValidator
                                label="Genre"
                                className={classes.textField}
                                value={itemToEdit.Genre}
                                onChange={this.handleChange('Genre')}
                                margin="normal"
                                name="Genre"
                            />
                            <TextValidator
                                label="Director"
                                className={classes.textField}
                                value={itemToEdit.Director}
                                onChange={this.handleChange('Director')}
                                margin="normal"
                                name="Director"
                            />
                            <TextValidator
                                label="Poster"
                                className={classes.textField}
                                value={itemToEdit.Poster}
                                onChange={this.handleChange('Poster')}
                                margin="normal"
                                name="Poster"
                            />
                            <TextValidator
                                label="Description"
                                className={classes.textField}
                                value={itemToEdit.Plot}
                                onChange={this.handleChange('Plot')}
                                margin="normal"
                                multiline={true}
                                name="Plot"
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Close
                            </Button>
                            <Button type="submit" color="primary" autoFocus>
                                Save
                            </Button>
                        </DialogActions>
                    </ValidatorForm>
                </Dialog>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        itemModal: state.moviesList.itemModal || false,
        itemToEdit: state.moviesList.itemToEdit || {},
        type: state.moviesList.actionEdit || false,
        error: state.moviesList.error,
    }
}

const mapDispatchToProps = dispatch => ({
    addItem: item => dispatch(addItem(item)),
    updateItem: item => dispatch(updateItem(item)),
    closeModal: () => dispatch(closeAddEditModal())
});

ModalAddEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};

const Container = withStyles(styles)(ModalAddEdit);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Container);
