import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import connect from "react-redux/es/connect/connect";
import {openDeleteModal, openAddEditModal} from '../../actions';

const styles = {
    media: {
        height: 445,
    },
};

class Content extends React.Component {
    handleDeleteModalOpen = event => {
        event.preventDefault();
        this.props.openDeleteModal(this.props.data.imdbID);
    };

    handleAddEditModalOpen = event => {
        event.preventDefault();
        this.props.openAddEditModal(this.props.data);
    };

    render() {
        const {classes, data} = this.props;
        return (
            <Card>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={data.Poster || '/image-placeholder.png'}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {data.Title}
                        </Typography>
                        <Typography component="p">Year: {data.Year}</Typography>
                        <Typography component="p">Runtime: {data.Runtime}</Typography>
                        <Typography component="p">Genre: {data.Genre}</Typography>
                        <Typography component="p">Director: {data.Director}</Typography>
                        <Typography component="p">Description: {data.Plot}</Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button
                        onClick={this.handleAddEditModalOpen}
                        size="small"
                        color="primary">
                        Edit
                    </Button>
                    <Button
                        onClick={this.handleDeleteModalOpen}
                        size="small"
                        color="primary">
                        Delete
                    </Button>
                </CardActions>
            </Card>
        );
    }

}

Content.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => ({
    openDeleteModal: id => dispatch(openDeleteModal(id)),
    openAddEditModal: item => dispatch(openAddEditModal(item))
});

const Container = withStyles(styles)(Content);

export default connect(
    null,
    mapDispatchToProps
)(Container);
