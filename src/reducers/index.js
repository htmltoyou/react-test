import { combineReducers } from 'redux'
import modalDelete from './modalDelete'
import moviesList from './moviesList'

export default combineReducers({
    moviesList,
    modalDelete
})
