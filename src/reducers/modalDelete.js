const initialState = {
    isOpen: false,
    id: null
};

const modalDelete = (state = initialState, action) => {
    switch (action.type) {
        case 'OPEN_DELETE_MODAL':
            return {
                ...state,
                isOpen: true,
                id: action.id
            };
        case 'CLOSE_DELETE_MODAL':
            return {
                ...state,
                isOpen: false,
                id: null
            };
        default:
            return state
    }
};

export default modalDelete
