const initialState = {
    list: [],
    error: {},
    itemModal: false,
    itemToEdit: null,
    noImageUrl: '/image-placeholder.png',
    defaultItem: {
        Director: '',
        Genre: '',
        Poster: '',
        Runtime: '',
        Title: '',
        Year: '',
        imdbID: Date.now(),
        titleChanged: false
    }
};

const moviesList = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_ITEM':
            const title = state.list.filter(item => {
                return item.Title === action.item.Title;
            });
            if (!title.length) {
                return {
                    ...state,
                    list: [...state.list, action.item],
                    itemModal: false,
                    error: {}
                };
            } else {
                return {
                    ...state,
                    itemToEdit: action.item,
                    error: {
                        name: 'This title exist'
                    }
                };
            }

        case 'UPDATE_ITEM':
            let newTittle = state.list.filter(item => {
                if (item.imdbID !== action.item.imdbID) {
                    return item.Title === action.item.Title;
                }
                return false;
            });
            if (!newTittle.length) {
                return {
                    ...state,
                    list: state.list.map(
                        item =>
                            item.imdbID === action.item.imdbID ? action.item : item
                    ),
                    itemModal: false
                };
            } else {
                return {
                    ...state,
                    itemToEdit: action.item,
                    error: {
                        name: 'title exist'
                    }
                };
            }
        case 'DELETE_ITEM':
            return {
                ...state,
                list: state.list.filter(
                    item => item.imdbID !== action.id
                ),
                error: {}
            }
        case 'SEARCH_ERROR':
            return {
                ...state,
                list: [],
                error: action.error
            };
        case 'SAVE_ITEM':
            return {
                ...state,
                list: action.searchResults,
                error: {}
            };
        case 'OPEN_ADD_EDIT_MODAL':
            if (!action.item.imdbID) {
                state.defaultItem.imdbID = Date.now()
            }
            return {
                ...state,
                itemModal: true,
                itemToEdit: action.item.imdbID ? action.item : state.defaultItem,
                actionEdit: !!action.item.imdbID,
                error: {}
            };
        case 'CLOSE_ADD_EDIT_MODAL':
            return {
                ...state,
                itemModal: false
            };

        default:
            return state
    }
};

export default moviesList
