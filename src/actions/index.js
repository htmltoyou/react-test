import * as types from './actionTypes';
import axios from "axios";

const apiKey = 'BanMePlz';
const baseUrl = 'https://www.omdbapi.com/';


export const openDeleteModal = id => {
    return {
        type: types.OPEN_DELETE_MODAL,
        id
    };
};

export const closeDeleteModal = () => {
    return {
        type: types.CLOSE_DELETE_MODAL
    };
};

export const openAddEditModal = item => {
    return {
        type: types.OPEN_ADD_EDIT_MODAL,
        item
    };
};

export const closeAddEditModal = () => {
    return {
        type: types.CLOSE_ADD_EDIT_MODAL
    };
};

export const addItem = item => {
    return {
        type: types.ADD_ITEM,
        item
    };
};

export const updateItem = item => {
    return {
        type: types.UPDATE_ITEM,
        item
    };
};

export const deleteItem = id => {
    return {
        type: types.DELETE_ITEM,
        id
    };
};

function saveSearch(searchResults) {
    return {
        type: types.SAVE_ITEM,
        searchResults
    };
}

function receiveError(error) {
    return {
        type: types.SEARCH_ERROR,
        error
    }
}

export const getItemData = (data) => {
    let listForFetch = [];
    let res = [];
    data.map(item => {
        listForFetch.push(axios.get(baseUrl, {
            params: {
                i: item.imdbID,
                apiKey: apiKey
            }
        }));
        return listForFetch;
    });

    return (dispatch) => {
        axios.all(listForFetch).then(results => {
            results.forEach(response => {
                res.push(response.data);
            });
            dispatch(saveSearch(res));
        });
    }
};

export const searchData = (data) => {
    return (dispatch) => {
        axios.get(baseUrl, {
            params: {
                s: data.title,
                y: data.year,
                apiKey: apiKey
            }
        }).then(response => {
            if (!response.data.Error) {
                dispatch(getItemData(response.data.Search));
            } else {
                dispatch(receiveError(response.data));
            }
        })
    };
};

